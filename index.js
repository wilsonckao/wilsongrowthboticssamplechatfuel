'use strict';
/** Loading Variables **/
require('dotenv').load();

/** Pre initialising BotBase Stuff */
const BotBase = require('botbase');
const Project_Config = require('./Config');

const FormStage = require('botbase-stage-form');
FormStage.addCustomizer(FormStage.CustomizerNames.ACTIONS, './custom/Actions.js');

const logger = BotBase.Logger(module);

const bb = new BotBase(Project_Config);

FormStage.addFormFileName('./dialogs/main.xml');

bb.addStage(require('./stages/IntroStage'));
bb.addStage(FormStage);

bb.start([BotBase.Int.Facebook]);