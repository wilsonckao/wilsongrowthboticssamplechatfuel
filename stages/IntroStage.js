const BotBase = require('botbase');
const FormStage = require('botbase-stage-form');
const logger = BotBase.Logger(module);

const InterStageCommunication = FormStage.InterStageCommunication;

class IntroStage extends BotBase.Stage {

	processText(text) {
		switch (text.rendered) {
			case 'reset':
				return new BotBase.Obj.Payload(InterStageCommunication.JUMP_TO, {form: 'main'})
		}

		return text;
	}
}

module.exports = IntroStage;